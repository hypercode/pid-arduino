//
// Created by dejan on 9. 08. 19.
//

#ifndef DRON_PIDSETTING_H
#define DRON_PIDSETTING_H

#include "Arduino.h"


enum Axis {
    undefined = -1,
    roll = 0,
    pitch = 1,
    yaw = 2,
    thrust = 3
};


template<class Output, class Factors>
class PIDsetting {
public:

    /**
     * Default constructor
     *
     */
    PIDsetting() {}


    /**
     * Constructor.
     *
     * @overload
     * @param axe
     */
    explicit PIDsetting(Axis axe) {
        _axe = axe;
    }

    /**
     * Constructor
     *
     * @overload
     * @param axe_index index of axe (that is defined in enum Axis)
     */
    explicit PIDsetting(int axe_index) {
        _axe = Axis(axe_index);
    }


    /**
     * Constructor with basic settings
     *
     * @overload
     * @param axe
     * @param p - proportional factor
     * @param i - integral factor
     * @param d - derivative factor
     */
    PIDsetting(Axis axe, Factors p, Factors i, Factors d) {
        _axe = axe;

        _proportional = p;
        _integral = i;
        _derivative = d;
    }


    /**
     * Constructor with basic settings
     *
     * @overload
     * @param p - proportional factor
     * @param i - integral factor
     * @param d - derivative factor
     */
    PIDsetting(Factors p, Factors i, Factors d) {
        _axe = Axis::undefined;

        _proportional = p;
        _integral = i;
        _derivative = d;
    }

    /**
     * Constructor with all settings
     *
     * @overload
     * @param axe
     * @param p - proportional factor
     * @param i - integral factor
     * @param d - derivative factor
     * @param min - minimum value of output
     * @param max  - max value of output
     */
    PIDsetting(Axis axe, Factors p, Factors i, Factors d, Output min, Output max) {
        _axe = axe;

        _proportional = p;
        _integral = i;
        _derivative = d;
        _minimum_value = min;
        _maximum_value = max;
    }

    /**
     * Constructor
     *
     * @overload
     * @param p - proportional factor
     * @param i - integral factor
     * @param d - derivative factor
     * @param min - minimum value of output
     * @param max  - max value of output
   */

    PIDsetting(Factors p, Factors i, Factors d, Output min, Output max) {
        _axe = Axis::undefined;

        _proportional = p;
        _integral = i;
        _derivative = d;
        _minimum_value = min;
        _maximum_value = max;
    }


    /// Axe that will be PID controller responsible for.
    Axis _axe = Axis::undefined;

    /// Factors, negative value means undefined.
    Factors _proportional = -1;
    Factors _integral = -1;
    Factors _derivative = -1;

    /// minimum and maximum values
    Output _minimum_value;
    Output _maximum_value;

};


#endif //DRON_PIDSETTING_H
