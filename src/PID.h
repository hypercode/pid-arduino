//
// Created by Dejan on 27. 05. 2019.
//

#ifndef MASTER_BRANCH_PID_H
#define MASTER_BRANCH_PID_H

#include "Arduino.h"
#include "PIDsetting.h"



/**
 * PID controller class
 * @tparam Data
 * type of output data
 *
 * @tparam Factor
 * Type of factors (Kp, Ki, Kd)
 */
template<class Data, class Factor>
class PID {
public:

    /**
     * Constructor
     *
     * @param settings
     * PIDsetting object must have same templeate.
     */
    PID(PIDsetting<Data, Factor> *settings);

    /**
     * Set new setpoint for the controller
     *
     * @param new_setpoint
     */
    void setSetPoint(Data new_setpoint);

    /**
     * Set new output limits for the controller
     *
     * @deprecated
     *
     * @param minimum
     * @param max
     */
    void setLimits(Data minimum, Data max);

    /**
     * Get output for that cyle
     *
     * @param input
     *
     * @return output of pid contoller
     */
    Data getOutput(Data input);


    /**
     * Retuns current setpoint
     * @return
     */
    Data getSetPoint();

    /**
     * Clears history of previous errors, sets time to 0.
     */
    void clearHistory();

    /// PIDsetting for the controller
    PIDsetting<float, float> *settings;

private:

    bool _firstRun = true;

    /// Current setpoint for the controller
    Data _setPoint = 0;

    /// Previous error
    Data _prevError = 0;

    /// Integral of error over time
    Data _integral_value = 0;

    /// Last millis() time, when PID was updated
    unsigned long _lastInputTime = 0;


};

template<class Data, class Factor>
PID<Data, Factor>::PID(PIDsetting<Data, Factor> *newsettings) {
    settings = newsettings;
}

template<class Data, class Factors>
void PID<Data, Factors>::setSetPoint(Data new_setpoint) {
    _setPoint = new_setpoint;
}

template<class Data, class Factors>
Data PID<Data, Factors>::getOutput(Data input) {

    Data error = _setPoint - input;

    Data deltaTime = (Data) (millis() - _lastInputTime) / 1000; //delta time in seconds

    // On first run, disable controller timing options, to
    // get accurate time from cycle to cycle (and not from start to first cycle)
    if(_firstRun){
        deltaTime = 0;
        _firstRun = false;
    }

    // Calculate the integral over error
    _integral_value = constrain(error * deltaTime + _integral_value, settings->_minimum_value, settings->_maximum_value);

    Data porpotional = settings->_proportional * error;
    Data integral = _integral_value * settings->_integral; //calc surface area of error(time) graph
    Data dirivative = (settings->_derivative * ((error - _prevError) / deltaTime));

    _prevError = error;

    if (settings->_axe == Axis::roll) {
        //  Serial.print(millis());

        Serial.println(error);

        Serial.print("dT:");
        Serial.print((deltaTime * 1000));
        /*Serial.print(" e:");
        Serial.print(error);
        Serial.print(" p:");
        Serial.print(porpotional);
        Serial.print(" i:");
        Serial.print(integral);
        Serial.print(" d:");
        Serial.print(dirivative);
        Serial.print('|');*/
    } else if (settings->_axe == Axis::pitch) {

        /* Serial.print("|e:");
         Serial.print(error);
         Serial.print(" p:");
         Serial.print(porpotional);
         Serial.print(" i:");
         Serial.print(integral);
         Serial.print(" d:");
         Serial.print(dirivative);
         Serial.print('|');*/
    }


    _lastInputTime = millis();

    return constrain(porpotional + integral + dirivative, settings->_minimum_value, settings->_maximum_value);
}

template<class Data, class Factors>
Data PID<Data, Factors>::getSetPoint() {
    return _setPoint;
}

template<class Data, class Factors>
void PID<Data, Factors>::clearHistory() {
    _integral_value =  0;
    _prevError = 0;
    _firstRun = true;
}

template<class Data, class Factors>
void PID<Data, Factors>::setLimits(Data min, Data max) {
    settings->_maximum_value = max;
    settings->_minimum_value = min;
    _integral_value = constrain(_integral_value, min, max);
}




#endif //MASTER_BRANCH_PID_H



